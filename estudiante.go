package main

import (
	"errors"
	"time"
)

//Estructura de estudiante
type Student struct {
	ID        int
	Name      string
	Age       int16
	Active    bool
	CreatedAt time.Time
	Updated   time.Time
}

//Create
func Create(e Student) error {
	q := `INSERT INTO estudiantes (name, age, active) VALUES ($1, $2, $3)`
	db := getConnection()
	defer db.Close()
	stmt, err := db.Prepare(q)
	if err != nil {
		return err
	}
	defer stmt.Close()
	r, err := stmt.Exec(e.Name, e.Age, e.Active)
	if err != nil {
		return err
	}

	i, _ := r.RowsAffected()
	if i != 1 {
		return errors.New("Se esperaba una fila afectada.")
	}
	return nil
}
